﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Events_1
{
    //class that will raise a simple event
    class Events_Demo
    {
        //lets use the event option for getting the delegate
        //just a note event is not a replacement for delegates. rather a wrapper around the delegates.

        //here I am, by default, assigning OnChange to a empty delegate function. 
        //this way, I dont have to check if OnChange is null
        //it is always pointing at something. 
        public event EventHandler OnChange = delegate { };

        public void raise_the_event()
        {
            //EventHandler delegate needs two parameters
            //the first one - this beloew - is the sender
            //the second one is of type EventArgs
            OnChange(this, new EventArgs());
        }

    }


    class Program
    {
    


        static void Main(string[] args)
        {

            //now let me crate an instance of Events_Demo
            Events_Demo temp_events_1 = new Events_Demo();

            //subscribing to the anonymous method
            temp_events_1.OnChange += (sender,e) =>
            {
                Console.WriteLine("event has been raised with value ");
            };

            temp_events_1.OnChange += (sender, e) =>
            {
                Console.WriteLine("another event has been raised with value ");
            };

            temp_events_1.raise_the_event();

            //lets stop the console from vanishing
            Console.ReadLine();
        }

        private static void Temp_events_1_OnChange(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }
    }
}
